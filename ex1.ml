module IntQueue = struct
  type t = int list

  exception E

  let empty = []
  let enq q x = q @ [x]
  let is_empty q = q = []

  let deq q =
    match q with
    | []     -> raise E
    | h :: t -> (h, t)

  let rec print q =
    match q with
    | []     -> print_string "\n"
    | h :: t -> print_int h ; print_string " " ; print t
end

let rec map f l =
  match l with
  | []       -> []
  | hd :: tl -> f hd :: map f tl

let rec filter p l =
  match l with
  | []       -> []
  | hd :: tl -> if p hd then hd :: filter p tl else filter p tl

let rec fold_right f l a =
  match l with
  | []       -> a
  | hd :: tl -> f hd (fold_right f tl a)

let rec fold_left f a l =
  match l with
  | []       -> a
  | hd :: tl -> fold_left f (f a hd) tl

(* P1 *)
let rec range : int -> int -> int list =
  fun n m -> if n <= m then n :: range (n + 1) m else []

let p1 = assert (range 3 7 = [3; 4; 5; 6; 7])

(* P2 *)
let rec concat : 'a list list -> 'a list =
  fun ll ->
  match ll with
  | []       -> []
  | hd :: tl -> hd @ concat tl

let p2 = assert (concat [[1; 2]; [3; 4; 5]] = [1; 2; 3; 4; 5])

(* P3 *)
let rec zipper : int list -> int list -> int list =
  fun al bl ->
  match al with
  | []       -> bl
  | ah :: at -> (
      match bl with
      | []       -> al
      | bh :: bt -> ah :: bh :: zipper at bt )

let p3 =
  assert (zipper [1; 3; 5] [2; 4; 6] = [1; 2; 3; 4; 5; 6]) ;
  assert (zipper [1; 3] [2; 4; 6; 8] = [1; 2; 3; 4; 6; 8]) ;
  assert (zipper [1; 3; 5; 7] [2; 4] = [1; 2; 3; 4; 5; 7])

(* P4 *)
let rec unzip : ('a * 'b) list -> 'a list * 'b list =
  fun l ->
  match l with
  | []       -> ([], [])
  | hd :: tl ->
    let a, b = hd in
    let al, bl = unzip tl in
    (a :: al, b :: bl)

let p4 =
  assert (
    unzip [(1, "one"); (2, "two"); (3, "three")]
    = ([1; 2; 3], ["one"; "two"; "three"]) )

(* P5 *)
let rec drop : 'a list -> int -> 'a list =
  fun l n ->
  if n = 0 then l
  else
    match l with
    | []       -> []
    | hd :: tl -> drop tl (n - 1)

let p5 =
  assert (drop [1; 2; 3; 4; 5] 2 = [3; 4; 5]) ;
  assert (drop [1; 2] 3 = []) ;
  assert (drop ["C"; "Java"; "OCaml"] 2 = ["OCaml"])

(* P6 *)
let rec sigma : (int -> int) -> int -> int -> int =
  fun f a b -> fold_left (fun acc e -> acc + f e) 0 (range a b)

let p6 =
  assert (sigma (fun x -> x) 1 10 = 55) ;
  assert (sigma (fun x -> x * x) 1 7 = 140)

(* P7 *)
let rec iter : int * (int -> int) -> int -> int =
  fun (n, f) a -> if n = 0 then a else (iter (n - 1, f)) (f a)

let p7 =
  assert (
    let n = 7 in
    iter (n, fun x -> 2 + x) 0 = 2 * n )

(* P8 *)
let rec all : ('a -> bool) -> 'a list -> bool =
  fun p l -> fold_right (fun e acc -> p e && acc) l true

let p8 = assert (all (fun x -> x > 5) [7; 8; 9] = true)

(* P9 *)
let rec lst2int : int list -> int =
  fun l -> fold_left (fun acc e -> (acc * 10) + e) 0 l

let p9 = assert (lst2int [1; 2; 3] = 123)

(* P10 *)
let rec length l = fold_right (fun e acc -> 1 + acc) l 0
let rec reverse l = fold_left (fun acc e -> e :: acc) [] l
let rec is_all_pos l = fold_right (fun e acc -> e > 0 && acc) l true
let rec map f l = fold_right (fun e acc -> f e :: acc) l []

let rec filter p l =
  fold_right (fun e acc -> if p e then e :: acc else acc) l []

let p10 =
  assert (length [3; 2; 4; 5] = 4) ;
  assert (reverse [5; 7; 1; 9] = [9; 1; 7; 5]) ;
  assert (is_all_pos [3; 6; 9] = true) ;
  assert (map (fun n -> n * 3) [2; 4; 6] = [6; 12; 18]) ;
  assert (filter (fun n -> n mod 3 = 1) (range 1 10) = [1; 4; 7; 10])

(* P11 *)
type nat = ZERO | SUCC of nat

let rec natadd : nat -> nat -> nat =
  fun a b ->
  match a with
  | ZERO    -> b
  | SUCC am -> natadd am (SUCC b)

let natmul : nat -> nat -> nat =
  fun a b ->
  let rec natmulimpl n acc =
    match n with
    | ZERO    -> acc
    | SUCC nm -> natmulimpl nm (natadd b acc) in
  natmulimpl a ZERO

let p11 =
  let two = SUCC (SUCC ZERO) in
  let three = SUCC (SUCC (SUCC ZERO)) in
  assert (natmul two three = SUCC (SUCC (SUCC (SUCC (SUCC (SUCC ZERO)))))) ;
  assert (natadd two three = SUCC (SUCC (SUCC (SUCC (SUCC ZERO)))))

(* P12 *)
type aexp =
  | Const of int
  | Var   of string
  | Power of string * int
  | Times of aexp list
  | Sum   of aexp list

let rec diff f x =
  match f with
  | Const n -> Const 0
  | Var y when x = y -> Const 1
  | Var _ -> Const 0
  | Power (y, n) when x = y -> (
      match n with
      | 2 -> Times [Const n; Var y]
      | 1 -> Const n
      | 0 -> Const 0
      | _ -> Times [Const n; Power (y, n - 1)] )
  | Power _ -> Const 0
  | Times [] -> Const 0
  | Times [hd] -> diff hd x
  | Times (hd :: tl) ->
    Sum [Times (diff hd x :: tl); Times [hd; diff (Times tl) x]]
  | Sum l -> Sum (fold_right (fun e acc -> diff e x :: acc) l [])
