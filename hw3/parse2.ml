(* cfg: variables, terminals, start_variable, productions *)
type symbol = T of string | N of string | Dot | Epsilon | End
type prod = symbol * symbol list
type production = prod list
type cfg = symbol list * symbol list * symbol * production

(* dfa: states, delta, initial_state, alias_of_states *)
type state = prod list
type delta = (state * symbol, state) BatMap.t
type dfa = state BatSet.t * delta * state * (state, int) BatMap.t

(* parsing_table: ([state, symbol] -> action), initial_state *)
type action = Shift of state | Goto of state | Reduce of prod | Accept
type parse_table = (state * symbol, action) BatMap.t * state


let ( |> ) x f = f x (* pipe operator *)
exception LogicError of string


(* stringinize functions *)
module Stringinize = struct
  let string_of_symbol s =
    match s with T v -> v | N v -> "_" ^ v | Dot -> "." | Epsilon -> "<e>" | End -> "$"

  let string_of_symbol_list l = String.concat " " (List.map string_of_symbol l)

  let string_of_symbol_set  s = string_of_symbol_list (BatSet.to_list s)

  let string_of_prod (pf, pt) = string_of_symbol pf ^ " -> " ^ string_of_symbol_list pt

  let string_of_prod_list pl =
    "[\n  " ^ String.concat "\n  " (List.map string_of_prod pl) ^ "\n]"

  let string_of_prod_set  ps =
    "{\n  " ^ String.concat "\n  " (List.map string_of_prod (BatSet.to_list ps)) ^ "\n}"

  let string_of_dfa (states, delta, init, alias) =
    let get_alias s = try BatMap.find s alias with _ -> -999 in
    let str = ref "" in
    str := !str ^ "################### DFA #####################\n" ;
    str := !str ^ "------------------ STATES -------------------\n" ;
    states |> BatSet.iter (fun s ->
      if get_alias s <> -1 then
        str := !str ^ string_of_int (get_alias s) ^ " : " ^ string_of_prod_list s ^ "\n"
    ) ;
    str := !str ^ "------------------ DELTA --------------------\n" ;
    delta |> BatMap.iter (fun (i, x) j ->
      if get_alias i <> -1 && get_alias j <> -1 then
        str := !str ^ string_of_int (get_alias i) ^ "  "
                    ^ string_of_symbol x          ^ "  ->  "
                    ^ string_of_int (get_alias j) ^ "\n"
    ) ;
    str := !str ^ "#############################################\n" ;
    !str

  let string_of_parsing_table (_, _, _, alias) t =
    let get_alias s = try BatMap.find s alias with _ -> -999 in
    let string_of_action a =
      match a with
      | Shift  s -> "shift "  ^ string_of_int (get_alias s)
      | Goto   s -> "goto "   ^ string_of_int (get_alias s)
      | Reduce p -> "reduce " ^ string_of_prod p
      | Accept   -> "accept"
    in
    let str = ref "" in
    t |> BatMap.iter (fun (i, x) a ->
      str := !str ^ string_of_int (get_alias i) ^ "  "
                  ^ string_of_symbol x          ^ "  ->  "
                  ^ string_of_action a          ^ "\n"
    ) ;
    !str
end
open Stringinize


(* returns { FIRST(X) | all X in (variables union terminals) } *)
let first_set : cfg -> (symbol, symbol BatSet.t) BatMap.t =
 fun (vars, terms, start_var, prods) ->

  let firsts = ref BatMap.empty in
  let get_first x   = try BatMap.find x !firsts with _ -> BatSet.empty in
  let in_first  x c = BatSet.mem c (get_first x) in
  let add_first x c = firsts := BatMap.add x (BatSet.add c (get_first x)) !firsts in

  List.iter (fun x -> add_first x x) terms ;

  let rec loop () =
    let changed =
      prods |> List.map (fun (x, ys) ->
        let changed = ref false in
        let add_first_watch x c = if not (in_first x c) then ( changed := true ; add_first x c ) in
        let rec iter_y ys =
          match ys with
          | [] -> add_first_watch x Epsilon
          | y :: ys ->
              if in_first y Epsilon
              then iter_y ys
              else get_first y |> BatSet.iter (fun c -> add_first_watch x c)
        in
        iter_y ys ;
        !changed
      ) |> List.fold_left ( || ) false
    in
    if changed then loop ()
  in
  loop () ;

  !firsts


(* returns { FOLLOW(X) | all X in variables } *)
let follow_set : cfg -> (symbol, symbol BatSet.t) BatMap.t =
 fun cfg ->
  let vars, terms, start_var, prods = cfg in

  let firsts = first_set cfg in
  let get_first x   = try BatMap.find x firsts with _ -> BatSet.empty in
  let in_first  x c = BatSet.mem c (get_first x) in
  let get_first_list xs =
    let fs = ref BatSet.empty in
    let rec loop xs =
      match xs with
      | [] ->
          fs := BatSet.add Epsilon !fs
      | x :: xs ->
          fs := BatSet.union (get_first x) !fs ;
          if in_first x Epsilon then
            loop xs
    in
    loop xs ;
    !fs
  in

  let follows = ref BatMap.empty in
  let get_follow x   = try BatMap.find x !follows with _ -> BatSet.empty in
  let in_follow  x c = BatSet.mem c (get_follow x) in
  let add_follow x c = follows := BatMap.add x (BatSet.add c (get_follow x)) !follows in

  add_follow start_var End ;

  let rec loop () =
    let changed =
      prods |> List.map (fun (a, ys) ->
        let changed = ref false in
        let add_follow_watch x c = if not (in_follow x c) then ( changed := true ; add_follow x c ) in
        let rec iter_y ys =
          match ys with
          | [] -> ()
          | b :: ys ->
              ( match b with
                | N _ ->
                    let first_beta = get_first_list ys in
                    first_beta |> BatSet.iter (fun el ->
                      if el <> Epsilon then
                        add_follow_watch b el
                    ) ;
                    if BatSet.mem Epsilon first_beta then
                      get_follow a |> BatSet.iter (fun el ->
                        add_follow_watch b el
                      )
                | _ -> () ) ;
              iter_y ys
        in
        iter_y ys ;
        !changed
      ) |> List.fold_left ( || ) false
    in
    if changed then loop ()
  in
  loop () ;
  !follows


(* returns CLOSURE(I) | I is state of cfg *)
let closure : cfg -> state -> state =
 fun (_, _, _, prods) i ->
  let i, queue = (BatSet.empty, i) in
  let rec loop i queue =
    match queue with
    | [] -> i
    | p :: queue ->
        if BatSet.mem p i then
          loop i queue
        else
          let i = BatSet.add p i in
          let pf, pt = p in
          let rec loop2 pt =
            match pt with
            | dot :: pt      when dot <> Dot -> loop2 pt
            | dot :: b :: pt when dot =  Dot ->
                let to_add =
                  prods
                  |> List.filter (fun (pef, pet) -> pef = b)
                  |> List.map    (fun (pef, pet) -> (pef, Dot :: pet))
                in
                loop i (queue @ to_add)
            | _ ->
                loop i queue
          in
          loop2 pt
  in
  loop i queue |> BatSet.to_list


(* returns GOTO(I, X) | I is state of cfg, X is terminal *)
let goto : cfg -> prod list -> symbol -> prod list =
 fun cfg i x ->
  let j = i |> List.fold_left (fun acc (pf, pt) ->
      let rec loop pth ptt =
        match ptt with
        | dot :: ptt       when dot <> Dot           -> loop (pth @ [dot]) ptt
        | dot :: x' :: ptt when dot =  Dot && x = x' ->
            (pf, pth @ [x'; dot] @ ptt) :: acc
        | _ ->
            acc
      in
      loop [] pt
    ) []
  in
  closure cfg j


(* returns parsing DFA of cfg *)
let automaton : cfg -> dfa =
 fun cfg ->
  let vars, terms, start_var, prods = cfg in

  let init = closure cfg [(N "", [Dot; start_var])] in

  let t, e = (BatSet.empty, BatMap.empty) in
  let queue = [init] in

  let alias = BatMap.add init 0 BatMap.empty in
  let counter_var = ref 0 in
  let counter () = counter_var := !counter_var + 1 ; !counter_var in

  let rec loop t e queue alias =
    match queue with
    | [] -> (t, e, init, alias)
    | i :: queue ->
        if BatSet.mem i t then
          loop t e queue alias
        else
          let t = BatSet.add i t in
          let alias =
            if BatMap.mem i alias
            then alias
            else BatMap.add i (if i = [] then -1 else counter ()) alias
          in
          let queue, e =
            vars @ terms |> List.fold_left (fun (q, e) x ->
                let j = goto cfg i x in
                (q @ [j], BatMap.add (i, x) j e)
            ) (queue, e)
          in
          loop t e queue alias
  in
  loop t e queue alias


(* returns parsing table of cfg *)
let parsing_table : cfg -> parse_table =
 fun cfg ->
  let vars, terms, start_var, prods = cfg in
  let states, delta, initial_state, alias = automaton cfg in

  let follows = follow_set cfg in
  let in_follow x c = BatSet.mem c (try BatMap.find x follows with _ -> BatSet.empty) in

  let table = ref BatMap.empty in

  delta |> BatMap.iter (fun (i, x) j ->
    ( if i <> [] && j <> [] then
        match x with
        | T _ -> table := BatMap.add (i, x) (Shift j) !table
        | N _ -> table := BatMap.add (i, x) (Goto  j) !table
        | _ -> ()
    ) ;

    if List.exists (fun p -> p = (N "", [start_var; Dot])) i then
      table := BatMap.add (i, End) Accept !table ;

    prods |> List.iter (fun prod ->
      let a, gamma = prod in
      let prod_dot = (a, gamma @ [Dot]) in

      if List.exists (fun p -> p = prod_dot) i then
        End :: terms |> List.iter (fun y ->
          if in_follow a y then
            if (try BatMap.find (i, y) !table with _ -> Reduce prod) <> Reduce prod then
              let get_alias s = try BatMap.find s alias with _ -> -999 in
              raise (LogicError (
                "SLR Parsing Table Conflict: ("
                  ^ string_of_int (get_alias i) ^ ", "
                  ^ string_of_symbol y          ^ ") -> "
                  ^ ( match BatMap.find (i, y) !table with
                      | Shift  s -> "shift "  ^ string_of_int (get_alias s)
                      | Reduce p -> "reduce " ^ string_of_prod p
                      | _ -> "???" 
                    )
                  ^ " / reduce " ^ string_of_prod prod
              ))
            else table := BatMap.add (i, y) (Reduce prod) !table
        )
    )
  ) ;
  (!table, initial_state)


(* check if cfg is SLR *)
let check_SLR : cfg -> bool = fun cfg ->
  try let _ = parsing_table cfg in true with _ -> false


(* parse sentence with cfg and returns success/fail *)
let parse : cfg -> symbol list -> bool =
 fun cfg sentence ->
  let table, init = parsing_table cfg in

  let rec loop stack symbols input =
    match stack with
    | top :: stack ->
      ( match input with
        | first :: input ->
          ( try
              let action = BatMap.find (top, first) table in
              ( match action with
                | Shift s ->
                    loop (s :: top :: stack) (symbols @ [first]) input
                | Reduce (pf, pt) -> (
                    let rec loop_replace stack syms_rev pt_rev =
                      match (stack, syms_rev, pt_rev) with
                      | _, _, [] ->
                          (stack, List.rev (pf :: syms_rev))
                      | top :: stack, sy :: syms_rev, sy' :: pt_rev when sy = sy' ->
                          loop_replace stack syms_rev pt_rev
                      | _ ->
                          raise (LogicError (
                            "Reduce failed: symbols " ^ string_of_symbol_list symbols
                                       ^ " / reduce " ^ string_of_prod (pf, pt)
                          ))
                    in
                    let stack, symbols =
                      loop_replace (top :: stack) (List.rev symbols) (List.rev pt)
                    in

                    match stack with
                    | prev_top :: stack -> (
                        let goto = BatMap.find (prev_top, pf) table in
                        match goto with
                        | Goto s ->
                            loop (s :: prev_top :: stack) symbols (first :: input)
                        | _ ->
                            false
                      )
                    | _ -> false
                  )
                | Accept ->
                    true
                | _ ->
                    false
              )
            with _ -> false
          )
        | _ -> false
      )
    | _ -> false
  in
  loop [init] [] sentence
