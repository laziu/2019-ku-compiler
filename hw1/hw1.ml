open Regex

exception Not_implemented

let regex2nfa : Regex.t -> Nfa.t =
 fun regex ->
  (* NFA with single initial/final state *)
  let new_nfa () =
    let nfa = Nfa.create_new_nfa () in
    let is = Nfa.get_initial_state nfa in
    let fs, nfa = Nfa.create_state nfa in
    let nfa = Nfa.add_final_state nfa fs in
    (nfa, is, fs) in
  (* NFA with the initial state, final states from existing NFA *)
  let get_nfa nfa =
    let is = Nfa.get_initial_state nfa in
    let fss = Nfa.get_final_states nfa in
    (nfa, is, fss) in
  (* union two NFA *)
  let merge_nfa nfa ts =
    let merge nfa t =
      let nfa = Nfa.add_states nfa (Nfa.get_states t) in
      let nfa = Nfa.add_edges nfa (Nfa.get_edges t) in
      nfa in
    List.fold_left merge nfa ts in
  (* impl: tree *)
  let rec convert rx =
    match rx with
    | Empty ->
        let nfa, _, _ = new_nfa () in
        nfa
    | Epsilon ->
        let nfa, is, fs = new_nfa () in
        let nfa = Nfa.add_epsilon_edge nfa (is, fs) in
        nfa
    | Alpha a ->
        let nfa, is, fs = new_nfa () in
        let nfa = Nfa.add_edge nfa (is, a, fs) in
        nfa
    | OR (r1, r2) ->
        let nfa1, is1, fs1s = get_nfa (convert r1) in
        let nfa2, is2, fs2s = get_nfa (convert r2) in
        let nfa, is, fs = new_nfa () in
        let es =
          [(is, is1); (is, is2)]
          @ BatSet.to_list (BatSet.map (fun fs1 -> (fs1, fs)) fs1s)
          @ BatSet.to_list (BatSet.map (fun fs2 -> (fs2, fs)) fs2s) in
        let nfa = merge_nfa nfa [nfa1; nfa2] in
        let nfa = Nfa.add_edges nfa ([], es) in
        nfa
    | CONCAT (r1, r2) ->
        let nfa1, is1, fs1s = get_nfa (convert r1) in
        let nfa2, is2, fs2s = get_nfa (convert r2) in
        let nfa, is, fs = new_nfa () in
        let es =
          [(is, is1)]
          @ BatSet.to_list (BatSet.map (fun fs1 -> (fs1, is2)) fs1s)
          @ BatSet.to_list (BatSet.map (fun fs2 -> (fs2, fs)) fs2s) in
        let nfa = merge_nfa nfa [nfa1; nfa2] in
        let nfa = Nfa.add_edges nfa ([], es) in
        nfa
    | STAR r1 ->
        let nfa1, is1, fs1s = get_nfa (convert r1) in
        let nfa, is, fs = new_nfa () in
        let es =
          (is, fs) :: (is, is1)
          :: BatSet.fold (fun fs1 l -> (fs1, is1) :: (fs1, fs) :: l) fs1s [] in
        let nfa = merge_nfa nfa [nfa1] in
        let nfa = Nfa.add_edges nfa ([], es) in
        nfa in
  convert regex

let nfa2dfa : Nfa.t -> Dfa.t =
 fun nfa ->
  (* check if the DFA state contains NFA final states *)
  let is_final_state ds = BatSet.exists (fun ns -> Nfa.is_final_state nfa ns) ds in
  (* union_{s in x} f(s) *)
  let union_for x f = BatSet.fold (fun s u -> BatSet.union (f s) u) x BatSet.empty in
  (* i + union_{s in i} delta(s, epsilon) *)
  let epsilon_closure i =
    let rec loop u w =
      match w with
      | s :: w ->
          let nss = Nfa.get_next_state_epsilon nfa s in
          let add_state ns (u, w) =
            if BatSet.mem ns u then (u, w)
            else
              let u = BatSet.add ns u in
              let w = ns :: w in
              (u, w) in
          let u, w = BatSet.fold add_state nss (u, w) in
          loop u w
      | _ -> u in
    let w = BatSet.to_list i in
    loop i w in
  (* impl: intialization *)
  let d0 = epsilon_closure (BatSet.of_list [Nfa.get_initial_state nfa]) in
  let d, w = (BatSet.of_list [d0], [d0]) in
  let dfa = Dfa.create_new_dfa d0 in
  let dfa = if is_final_state d0 then Dfa.add_final_state dfa d0 else dfa in
  (* impl: loop *)
  let rec loop (dfa, d, w) =
    match w with
    | q :: w ->
        let for_each_char (dfa, d, w) c =
          let t = epsilon_closure (union_for q (fun s -> Nfa.get_next_state nfa s c)) in
          let dfa, d, w =
            if BatSet.mem t d then (dfa, d, w)
            else
              let dfa =
                (if is_final_state t then Dfa.add_final_state else Dfa.add_state) dfa t in
              let d = BatSet.add t d in
              let w = t :: w in
              (dfa, d, w) in
          let dfa = Dfa.add_edge dfa (q, c, t) in
          (dfa, d, w) in
        loop (List.fold_left for_each_char (dfa, d, w) [A; B])
    | _ -> dfa in
  loop (dfa, d, w)

(* Do not modify this function *)
let regex2dfa : Regex.t -> Dfa.t =
 fun regex ->
  let nfa = regex2nfa regex in
  let dfa = nfa2dfa nfa in
  dfa

let run_dfa : Dfa.t -> alphabet list -> bool =
 fun dfa str ->
  let rec loop s cl =
    match cl with
    | c :: cl ->
        let ns = Dfa.get_next_state dfa s c in
        loop ns cl
    | _ -> Dfa.is_final_state dfa s in
  let is = Dfa.get_initial_state dfa in
  loop is str
