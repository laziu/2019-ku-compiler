type alphabet = A | B

let string_of_alphabet a = match a with A -> "a" | B -> "b"

type t = 
  | Empty 
  | Epsilon
  | Alpha of alphabet
  | OR of t * t
  | CONCAT of t * t
  | STAR of t

let string_of_regex : t -> string =
 fun regex ->
  let rec string_of_regex_impl r =
    match r with
    | Empty -> "Empty"
    | Epsilon -> "e"
    | Alpha a -> string_of_alphabet a
    | OR (r1, r2) -> "(" ^ string_of_regex_impl r1 ^ "|" ^ string_of_regex_impl r2 ^ ")"
    | CONCAT (r1, r2) -> string_of_regex_impl r1 ^ string_of_regex_impl r2
    | STAR r -> "(" ^ string_of_regex_impl r ^ ")*" in
  ("/" ^ string_of_regex_impl regex ^ "/")
