open Batteries

exception May_out_of_range
exception May_divide_by_zero
exception Use_of_uninitialized_value

let ( |> ) x f = f x

module Op = struct
  open BatBig_int

  let plot a =
    if compare a (of_int max_int) > 0 then max_int
    else if compare a (of_int min_int) < 0 then min_int
    else to_int a

  let minus a = plot (a |> of_int |> neg)
  let add a b = plot ((a |> of_int) + (b |> of_int))
  let sub a b = plot ((a |> of_int) - (b |> of_int))
  let mul a b = plot ((a |> of_int) * (b |> of_int))
  let div a b = plot ((a |> of_int) / (b |> of_int))

  let minmax li =
    match li with
    | [] -> raise (Invalid_argument "Op.minmax: nil value")
    | a :: bs -> List.fold_left (fun (pvmin, pvmax) n -> (min pvmin n, max pvmax n)) (a, a) bs

  let bitwise_list op (a1, a2) (b1, b2) = [op a1 b1; op a1 b2; op a2 b1; op a2 b2]
end

type exp_type = TVAR of S.id | CNUM of int

(* interval = from, to, step *)
(* var_state = { var_id -> possible_range } *)
(* arr_state = { arr_id -> possible_size  } *)
type cpo = Bot | Interval of int * int
type var_states = (S.id, cpo) BatMap.t
type arr_states = (S.id, cpo) BatMap.t
type ioset = var_states * arr_states

module Str = struct
  let cpo c =
    match c with Bot -> "BOT" | Interval (vmin, vmax) -> Printf.sprintf "[%d, %d]" vmin vmax

  let states cs =
    "{ "
    ^ ( []
      |> BatMap.foldi (fun id c ls -> (id ^ " -> " ^ cpo c) :: ls) cs
      |> List.rev |> String.concat "; " )
    ^ " }"

  let ioset (vars, arrs) = "V" ^ states vars ^ " | A" ^ states arrs
end

let top = Interval (min_int, max_int)

let inverse_cpo c =
  match c with
  | Bot -> top
  | Interval (rmin, rmax) ->
      if rmin <> min_int then if rmax <> max_int then top else Interval (min_int, rmin - 1)
      else if rmax <> max_int then Interval (rmax + 1, max_int)
      else Bot

let gcd na nb =
  let rec gcd_safe a b = if b = 0 then a else gcd_safe b (a mod b) in
  if na = 0 then nb else gcd_safe (abs na) (abs nb)

let get_cpo id states = try BatMap.find id states with _ -> Bot
let add_cpo = BatMap.add

let check_range size range =
  match (size, range) with
  | Interval (e1min, e1max), Interval (e2min, e2max) -> 0 <= e2min && e2max < e1min
  | _ -> raise Use_of_uninitialized_value

let union_cpo c1 c2 =
  match (c1, c2) with
  | Bot, _ -> c2
  | _, Bot -> c1
  | Interval (e1min, e1max), Interval (e2min, e2max) ->
      Interval (min e1min e2min, max e1max e2max)

let union_states cs1 cs2 =
  BatMap.merge
    (fun id c1' c2' ->
      match (c1', c2') with
      | None, _ -> c2'
      | _, None -> c1'
      | Some c1, Some c2 -> Some (union_cpo c1 c2))
    cs1 cs2

let widen_states cs1 cs2 =
  BatMap.merge
    (fun id c1' c2' ->
      match (c1', c2') with
      | None, _ -> c2'
      | _, None -> c1'
      | Some c1, Some c2 ->
          Some
            ( match (c1, c2) with
            | Bot, _ -> c2
            | _, Bot -> c1
            | Interval (pmin, pmax), Interval (nmin, nmax) ->
                Interval
                  ( (if pmin > nmin then min_int else pmin)
                  , if pmax < nmax then max_int else pmax ) ))
    cs1 cs2

let intersect_cpo c1 c2 =
  match (c1, c2) with
  | Bot, _ -> c2
  | _, Bot -> c1
  | Interval (e1min, e1max), Interval (e2min, e2max) ->
      let emin, emax = (max e1min e2min, min e1max e2max) in
      if emin > emax then Bot else Interval (emin, emax)

let restrict_state id c cs =
  BatMap.mapi (fun id' c' -> if id = id' then intersect_cpo c c' else c') cs

let intersect_states cs1 cs2 =
  BatMap.merge
    (fun id c1' c2' ->
      match (c1', c2') with
      | None, _ -> None
      | _, None -> None
      | Some c1, Some c2 -> Some (intersect_cpo c1 c2))
    cs1 cs2

let equal_states cs1 cs2 =
  BatMap.equal
    (fun c1 c2 ->
      match (c1, c2) with
      | Bot, Bot -> true
      | Bot, _ -> false
      | _, Bot -> false
      | Interval (e1min, e1max), Interval (e2min, e2max) -> e1min = e2min && e1max = e2max)
    cs1 cs2

let rec ioset_from_block blk ios = ios |> ioset_from_decls blk |> ioset_from_stmts blk

and ioset_from_decls (decls, _) ios =
  List.fold_left (fun ios decl -> ios |> ioset_from_decl decl) ios decls

and ioset_from_decl : S.decl -> ioset -> ioset =
 fun (typ, id) (vars, arrs) ->
  match typ with
  | TINT -> (vars |> add_cpo id (Interval (0, 0)), arrs)
  | TARR sz -> (vars, arrs |> add_cpo id (Interval (sz, sz)))

and ioset_from_stmts (_, stmts) ios =
  List.fold_left (fun ios stmt -> ios |> ioset_from_stmt stmt) ios stmts

and ioset_from_stmt : S.stmt -> ioset -> ioset =
 fun stmt ios ->
  let vars, arrs = ios in
  match stmt with
  | ASSIGN (lv, exp) -> (
      let c = cpo_from_exp exp ios in
      match lv with
      | ID id -> (vars |> add_cpo id c, arrs)
      | ARR (id, iexp) ->
          let range = cpo_from_exp iexp ios in
          let size = get_cpo id arrs in
          if check_range size range then ios else raise May_out_of_range )
  | IF (exp, st1, st2) ->
      let vt, vf = cut_of_1var_cond exp ios in
      let vs_t, as_t =
        if BatMap.is_empty vt then (vt, arrs) else (vt, arrs) |> ioset_from_stmt st1 in
      let vs_f, as_f =
        if BatMap.is_empty vf then (vf, arrs) else (vf, arrs) |> ioset_from_stmt st2 in
      (union_states vs_t vs_f, union_states as_t as_f)
  | WHILE (exp, st) ->
      let rec loop vs_p as_p =
        let vt, vf = cut_of_1var_cond exp (vs_p, as_p) in
        let vs_t, as_t =
          if BatMap.is_empty vt then (vt, as_p) else (vt, as_p) |> ioset_from_stmt st in
        if equal_states vs_p (union_states vs_p vs_t) then (intersect_states vs_t vf, as_t)
        else loop (widen_states vs_p vs_t) (widen_states as_p as_t) in
      loop vars arrs
  | DOWHILE (st, exp) ->
      let rec loop vs_p as_p =
        let vt, vf = cut_of_1var_cond exp (vs_p, as_p) in
        let vs_t, as_t =
          if BatMap.is_empty vt then (vt, as_p) else (vt, as_p) |> ioset_from_stmt st in
        if equal_states vs_p vs_t && equal_states as_p as_t then (vf, as_t)
        else loop (union_states vs_p vs_t) (union_states as_p as_t) in
      let vs_i, as_i = ios |> ioset_from_stmt st in
      loop (union_states vars vs_i) (union_states arrs as_i)
  | READ id -> (vars |> add_cpo id top, arrs)
  | PRINT exp ->
      let _ = cpo_from_exp exp ios in
      ios
  | BLOCK blk -> ioset_from_block blk ios

and cpo_from_exp : S.exp -> ioset -> cpo =
 fun exp ios ->
  let vars, arrs = ios in
  let bop_match e1 e2 fn =
    match (cpo_from_exp e1 ios, cpo_from_exp e2 ios) with
    | Interval (e1min, e1max), Interval (e2min, e2max) -> fn (e1min, e1max) (e2min, e2max)
    | _ -> raise Use_of_uninitialized_value in
  let uop_match e fn =
    match cpo_from_exp e ios with
    | Interval (emin, emax) -> fn (emin, emax)
    | _ -> raise Use_of_uninitialized_value in
  let tf_interval pt pf =
    match (pt, pf) with
    | true, true -> Interval (0, 1)
    | true, false -> Interval (1, 1)
    | false, true -> Interval (0, 0)
    | false, false -> Bot in
  let falsable emin emax = emin <= 0 && 0 <= emax in
  let trueable emin emax = not (emin = 0 && emax = 0) in
  let tf_able emin emax = (trueable emin emax, falsable emin emax) in
  match exp with
  | NUM n -> Interval (n, n)
  | LV lv -> (
    match lv with
    | ID id -> get_cpo id vars
    | ARR (id, iexp) ->
        let range = cpo_from_exp iexp ios in
        let size = get_cpo id arrs in
        if check_range size range then top else raise May_out_of_range )
  | ADD (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          Interval (Op.add e1min e2min, Op.add e1max e2max))
  | SUB (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          Interval (Op.sub e1min e2max, Op.sub e1max e2min))
  | MUL (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          let mn, mx = Op.minmax (Op.bitwise_list Op.mul (e1min, e1max) (e2min, e2max)) in
          Interval (mn, mx))
  | DIV (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          if e2min * e2max <= 0 then top
          else
            let mn, mx = Op.minmax (Op.bitwise_list Op.div (e1min, e1max) (e2min, e1max)) in
            Interval (mn, mx))
  | MINUS e -> uop_match e (fun (emin, emax) -> Interval (Op.minus emax, Op.minus emin))
  | NOT e ->
      uop_match e (fun (emin, emax) -> tf_interval (trueable emin emax) (falsable emin emax))
  | LT (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          tf_interval (e1min < e2max) (e1max >= e2min))
  | LE (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          tf_interval (e1min <= e2max) (e1max > e2min))
  | GT (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          tf_interval (e1max > e2min) (e1min <= e2max))
  | GE (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          tf_interval (e1max >= e2min) (e1min < e2max))
  | EQ (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          tf_interval (e1min <= e2max && e1max >= e2min) (e1max < e2min || e1min > e2max))
  | AND (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          let e1pt, e1pf = tf_able e1min e1max in
          let e2pt, e2pf = tf_able e2min e2max in
          tf_interval (e1pt && e2pt) (e1pf || e2pf))
  | OR (e1, e2) ->
      bop_match e1 e2 (fun (e1min, e1max) (e2min, e2max) ->
          let e1pt, e1pf = tf_able e1min e1max in
          let e2pt, e2pf = tf_able e2min e2max in
          tf_interval (e1pt || e2pt) (e1pf && e2pf))

and cut_of_1var_cond : S.exp -> ioset -> var_states * var_states =
 fun exp ios ->
  let vars, arrs = ios in
  let rec propagate ex (rmin, rmax) =
    match ex with
    | S.NUM n -> (vars, vars)
    | LV lv -> (
      match lv with
      | ID id ->
          let vt = restrict_state id (Interval (rmin, rmax)) vars in
          let vf =
            if rmin <> min_int && rmax <> max_int then
              union_states
                (restrict_state id (Interval (min_int, Op.sub rmin 1)) vars)
                (restrict_state id (Interval (Op.add rmax 1, max_int)) vars)
            else restrict_state id (inverse_cpo (Interval (rmin, rmax))) vars in
          (vt, vf)
      | ARR _ -> (vars, vars) )
    | ADD (e1, e2) -> (
      match (cpo_from_exp e1 ios, cpo_from_exp e2 ios) with
      | Interval (e1min, e1max), Interval (e2min, e2max) ->
          if e1min <> e1max && e2min <> e2max then (vars, vars)
          else if e1min <> e1max then propagate e1 (Op.sub rmin e2min, Op.sub rmax e2max)
          else if e2min <> e2max then propagate e2 (Op.sub rmin e1min, Op.sub rmax e1max)
          else (vars, vars)
      | _ -> (vars, vars) )
    | SUB (e1, e2) -> (
      match (cpo_from_exp e1 ios, cpo_from_exp e2 ios) with
      | Interval (e1min, e1max), Interval (e2min, e2max) ->
          if e1min <> e1max && e2min <> e2max then (vars, vars)
          else if e1min <> e1max then propagate e1 (Op.add rmin e2min, Op.add rmax e2max)
          else if e2min <> e2max then propagate e2 (Op.sub e1min rmax, Op.sub e2max rmin)
          else (vars, vars)
      | _ -> (vars, vars) )
    | MUL (e1, e2) -> (vars, vars) (* skip complex *)
    | DIV (e1, e2) -> (vars, vars)
    | MINUS e -> propagate e (Op.minus rmax, Op.minus rmin)
    | _ -> (vars, vars) in
  match exp with
  | NOT e ->
      let vt, vf = cut_of_1var_cond e ios in
      (vf, vt)
  | LT (e1, e2) -> (
    match (cpo_from_exp e1 ios, cpo_from_exp e2 ios) with
    | Interval (e1min, e1max), Interval (e2min, e2max) ->
        if e1max < e2min then (vars, BatMap.empty)
        else if e1min >= e2max then (BatMap.empty, vars)
        else
          let vs1t, vs1f = propagate e1 (min_int, Op.sub e2max 1) in
          let vs2t, vs2f = propagate e2 (Op.add e1min 1, max_int) in
          (intersect_states vs1t vs2t, intersect_states vs1f vs2f)
    | _ -> (vars, vars) )
  | LE (e1, e2) -> (
    match (cpo_from_exp e1 ios, cpo_from_exp e2 ios) with
    | Interval (e1min, e1max), Interval (e2min, e2max) ->
        if e1max <= e2min then (vars, BatMap.empty)
        else if e1min > e2max then (BatMap.empty, vars)
        else
          let vs1t, vs1f = propagate e1 (min_int, e2max) in
          let vs2t, vs2f = propagate e2 (e1min, max_int) in
          (intersect_states vs1t vs2t, intersect_states vs1f vs2f)
    | _ -> (vars, vars) )
  | GT (e1, e2) -> cut_of_1var_cond (LT (e2, e1)) ios
  | GE (e1, e2) -> cut_of_1var_cond (LE (e2, e1)) ios
  | EQ (e1, e2) -> (
    match (cpo_from_exp e1 ios, cpo_from_exp e2 ios) with
    | Interval (e1min, e1max), Interval (e2min, e2max) ->
        if e1max < e2min || e2max < e1min then (BatMap.empty, vars)
        else
          let vs1t, vs1f = propagate e1 (e2min, e2max) in
          let vs2t, vs2f = propagate e2 (e1min, e1max) in
          (intersect_states vs1t vs2t, intersect_states vs1f vs2f)
    | _ -> (vars, vars) )
  | AND (e1, e2) ->
      let vs1t, vs1f = cut_of_1var_cond e1 ios in
      let vs2t, vs2f = cut_of_1var_cond e2 ios in
      (intersect_states vs1t vs2t, union_states vs1f vs2f)
  | OR (e1, e2) ->
      let vs1t, vs1f = cut_of_1var_cond e1 ios in
      let vs2t, vs2f = cut_of_1var_cond e2 ios in
      (union_states vs1t vs2t, intersect_states vs1f vs2f)
  | _ -> (vars, vars)

let verify : S.program -> bool =
 fun s_pgm ->
  try
    let _ = ioset_from_block s_pgm (BatMap.empty, BatMap.empty) in
    true
  with e -> false
