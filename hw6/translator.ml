open Batteries

let temp_counter = ref 0
let temp_id () = temp_counter := !temp_counter + 1 ; ".t" ^ string_of_int !temp_counter

let label_counter = ref (T.dummy_label + 1)
let label () = label_counter := !label_counter + 1 ; !label_counter

let dlinstrize : T.instr -> T.linstr = fun instr -> (T.dummy_label, instr)

let translate_decl : S.decl -> T.linstr = fun (typ, id) ->
  (T.dummy_label, match typ with TINT -> COPYC (id, 0) | TARR sz -> ALLOC (id, sz))

let rec translate_exp : S.exp -> T.var * T.instr list = fun exp ->
  let assignv (var, bop, exp1, exp2) =
    let tvar1, tinstrs1 = translate_exp exp1 in
    let tvar2, tinstrs2 = translate_exp exp2 in
    tinstrs1 @ tinstrs2 @ [ASSIGNV (var, bop, tvar1, tvar2)]
  in
  let assignu (var, uop, exp) =
    let tvar, tinstrs = translate_exp exp in
    tinstrs @ [ASSIGNU (var, uop, tvar)]
  in
  let t = temp_id () in
  ( t
  , match exp with
    | NUM int -> [COPYC (t, int)]
    | LV lv ->
      ( match lv with
        | ID id -> [COPY (t, id)]
        | ARR (id, iexp) ->
            let idx, iinstrs = translate_exp iexp in
            iinstrs @ [LOAD (t, (id, idx))] )
    | ADD (exp1, exp2) -> assignv (t, ADD, exp1, exp2)
    | SUB (exp1, exp2) -> assignv (t, SUB, exp1, exp2)
    | MUL (exp1, exp2) -> assignv (t, MUL, exp1, exp2)
    | DIV (exp1, exp2) -> assignv (t, DIV, exp1, exp2)
    | MINUS exp -> assignu (t, MINUS, exp)
    | NOT   exp -> assignu (t, NOT  , exp)
    | LT (exp1, exp2) -> assignv (t, LT, exp1, exp2)
    | LE (exp1, exp2) -> assignv (t, LE, exp1, exp2)
    | GT (exp1, exp2) -> assignv (t, GT, exp1, exp2)
    | GE (exp1, exp2) -> assignv (t, GE, exp1, exp2)
    | EQ (exp1, exp2) -> assignv (t, EQ, exp1, exp2)
    | AND (exp1, exp2) -> assignv (t, AND, exp1, exp2)
    | OR  (exp1, exp2) -> assignv (t, OR , exp1, exp2)
  )

let rec translate_stmt : S.stmt -> T.linstr list = fun stmt ->
  match stmt with
  | ASSIGN (lv, exp) ->
      let tvar, tinstrs = translate_exp exp in
      List.map dlinstrize tinstrs
      @ ( match lv with
          | ID id -> [dlinstrize (COPY (id, tvar))]
          | ARR (id, iexp) ->
              let idx, iinstrs = translate_exp iexp in
              List.map dlinstrize iinstrs
              @ [dlinstrize (STORE ((id, idx), tvar))] )
  | IF (exp, stmt1, stmt2) ->
      let tvar, tinstrs = translate_exp exp in
      let l1, slinstrs1 = (label (), translate_stmt stmt1) in
      let l2, slinstrs2 = (label (), translate_stmt stmt2) in
      let lf = label () in
      List.map dlinstrize tinstrs
      @ [ dlinstrize (CJUMP (tvar, l1))
        ; dlinstrize (UJUMP l2)
        ; (l1, SKIP) ]
      @ slinstrs1
      @ [ dlinstrize (UJUMP lf)
        ; (l2, SKIP) ]
      @ slinstrs2
      @ [ dlinstrize (UJUMP lf)
        ; (lf, SKIP) ]
  | WHILE (exp, stmt) ->
      let tvar, tinstrs = translate_exp exp in
      let ls, slinstrs = (label (), translate_stmt stmt) in
      let lf = label () in
      [(ls, T.SKIP)]
      @ List.map dlinstrize tinstrs
      @ [dlinstrize (CJUMPF (tvar, lf))]
      @ slinstrs
      @ [ dlinstrize (UJUMP ls)
        ; (lf, SKIP) ]
  | DOWHILE (stmt, exp) ->
      let tvar, tinstrs = translate_exp exp in
      let ls, slinstrs = (label (), translate_stmt stmt) in
      [(ls, T.SKIP)]
      @ slinstrs
      @ List.map dlinstrize tinstrs
      @ [dlinstrize (CJUMP (tvar, ls))]
  | READ id -> [dlinstrize (READ id)]
  | PRINT exp ->
      let tvar, tinstrs = translate_exp exp in
      List.map dlinstrize tinstrs
      @ [dlinstrize (WRITE tvar)]
  | BLOCK (decls, stmts) ->
      List.map translate_decl decls
      @ List.concat (List.map translate_stmt stmts)

let translate : S.program -> T.program = fun (decls, stmts) ->
  List.map translate_decl decls
  @ List.concat (List.map translate_stmt stmts)
  @ [(T.dummy_label, T.HALT)]
