type symbol =
  | T of string (* terminal symbol; tokens *)
  | N of string (* nonterminal symbol; variables *)
  | Epsilon
  | End

type production = (symbol * symbol list) list
type cfg = symbol list * symbol list * symbol * production

let ( |> ) x f = f x (* pipe operator *)

let rec list_eq a b =
  match (a, b) with
  | [], [] -> true
  | [], _ -> false
  | _, [] -> false
  | ah :: at, bh :: bt -> if ah <> bh then false else list_eq at bt

let string_of_symbol s =
  match s with
  | T v -> "T<" ^ v ^ ">"
  | N v -> "N<" ^ v ^ ">"
  | Epsilon -> "e"
  | End -> "$"

let string_of_symlist l = l |> List.map string_of_symbol |> String.concat " "
let string_of_symset s = s |> BatSet.to_list |> string_of_symlist

exception LogicError of string

let first_cycle_error x =
  LogicError ("cycle found while calculating FIRST(" ^ string_of_symbol x ^ ")")
let multiple_table_entry_error k =
  let k1, k2 = k in
  LogicError ( "Multiple production in parsing_table[" ^ string_of_symbol k1 ^ "," ^ string_of_symbol k2 ^ "]" )
let stack_string_mismatch_error stack str =
  LogicError ("stack and sentence mismatch: " ^ string_of_symlist stack ^ " | " ^ string_of_symlist str)
let no_table_entry_error k =
  let k1, k2 = k in
  LogicError ("table[" ^ string_of_symbol k1 ^ "," ^ string_of_symbol k2 ^ "] not found")

let parsing_table : cfg -> (symbol * symbol, symbol list) BatMap.t =
 fun (vars, terms, start_var, productions) ->
  (* cache of first_of_symbol results *)
  let first_of_symbol_cache = ref BatMap.empty in
  (* checker to avoid cycle *)
  let first_of_symbol_check = ref BatSet.empty in
  (* FIRST(X) when X = symbol *)
  let rec first_of_symbol x =
    match x with
    | T _ -> BatSet.of_list [x]
    | N _ -> (
      try BatMap.find x !first_of_symbol_cache
      with _ ->
        if BatSet.mem x !first_of_symbol_check then raise (first_cycle_error x) ;
        first_of_symbol_check := BatSet.add x !first_of_symbol_check ;
        let res =
          productions
          |> List.filter (fun (from, _) -> from = x)
          |> List.map (fun (_, derived) -> first_of_string derived)
          |> List.fold_left (fun ss s -> BatSet.union ss s) BatSet.empty in
        first_of_symbol_cache := BatMap.add x res !first_of_symbol_cache ;
        first_of_symbol_check := BatSet.remove x !first_of_symbol_check ;
        res )
    | _ -> BatSet.empty
  (* FIRST(X) when X = symbol list *)
  and first_of_string x =
    match x with
    | [] -> BatSet.of_list [Epsilon]
    | y1 :: ys ->
        let first_y1 = first_of_symbol y1 in
        let r1 = first_y1 |> BatSet.remove Epsilon in
        let r2 = if BatSet.mem Epsilon first_y1 then first_of_string ys else BatSet.empty in
        BatSet.union r1 r2 in
  (* cache of follow results *)
  let follow_cache = ref BatMap.empty in
  (* checker to avoid cycle *)
  let follow_check = ref BatSet.empty in
  (* FOLLOW(v) *)
  let rec follow v =
    match v with
    | N _ -> (
      try BatMap.find v !follow_cache
      with _ ->
        if BatSet.mem v !follow_check then BatSet.empty
        else (
          follow_check := BatSet.add v !follow_check ;
          let r1 = if v = start_var then BatSet.of_list [End] else BatSet.empty in
          let rec loop (a, bs) =
            match bs with
            | [] -> BatSet.empty
            | b :: bs ->
                if v = b then
                  let first_bs = first_of_string bs in
                  let r2 = first_bs |> BatSet.remove Epsilon in
                  let r3 = if BatSet.mem Epsilon first_bs && a <> b then follow a else BatSet.empty in
                  BatSet.union r2 r3
                else loop (a, bs) in
          let res = productions |> List.map loop |> List.fold_left (fun ss s -> BatSet.union ss s) r1 in
          follow_cache := BatMap.add v res !follow_cache ;
          follow_check := BatSet.remove v !follow_check ;
          res ) )
    | _ -> raise (LogicError "FOLLOW should take non-terminal symbol") in
  (* helper function for construct *)
  let map_add k v t =
    if BatMap.mem k t && not (list_eq v (BatMap.find k t)) then
      raise (multiple_table_entry_error k) ;
    BatMap.add k v t in
  (* construct parsing table *)
  let rec construct table (v, alpha) =
    let first_alpha = first_of_string alpha in
    first_alpha |> BatSet.remove Epsilon |> BatSet.to_list
    |> List.fold_left
         (fun tab a -> map_add (v, a) alpha tab)
         ( if BatSet.mem Epsilon first_alpha then
           let follow_v = follow v in
           follow_v |> BatSet.remove Epsilon |> BatSet.to_list
           |> List.fold_left
                (fun tab b -> map_add (v, b) alpha tab)
                (if BatSet.mem End follow_v then map_add (v, End) alpha table else table)
         else table ) in
  List.fold_left construct BatMap.empty productions

let check_LL1 : cfg -> bool =
 fun cfg ->
  try let _ = parsing_table cfg in true
  with LogicError e -> false

let parse : cfg -> symbol list -> bool =
 fun cfg sentence ->
  try
    let table = parsing_table cfg in
    let vars, terms, start, prods = cfg in
    let rec loop stack str =
      match stack with
      | x :: xs -> (
        match str with
        | a :: ra -> (
            if x = a then loop xs ra
            else
              match x with
              | T _ -> raise (stack_string_mismatch_error stack str)
              | _ ->
                  let product = try BatMap.find (x, a) table with _ -> raise (no_table_entry_error (x, a)) in
                  loop (product @ xs) str )
        | _ -> raise (LogicError "Unreachable position; stack has multiple $") )
      | _ -> List.length str = 0 in
    loop [start; End] sentence
  with LogicError e -> false
