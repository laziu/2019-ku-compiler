exception UnreachableStatement
let ( |> ) x f = f x


let set_nth i v l = List.mapi (fun j w -> if j = i then v else w) l

let get_pc_of_label l2pc l = T.get_pc_of_label ([], l2pc) l
let next_pcs : T.l2pc -> T.pc -> T.instr -> T.pc list = fun l2pc pc instr ->
  match instr with
  | UJUMP      l  -> [get_pc_of_label l2pc l]
  | CJUMP  (_, l)
  | CJUMPF (_, l) -> [pc + 1; get_pc_of_label l2pc l]
  | HALT          -> []
  | _             -> [pc + 1]

let string_of_instr : T.instr -> string = fun instr ->
  let s_bop o = match o with T.ADD -> "+" | SUB -> "-"  | MUL -> "*"  | DIV -> "/"  | LT -> "<"
                | LE -> "<=" | GT  -> ">" | GE  -> ">=" | EQ  -> "==" | AND -> "&&" | OR -> "||" in
  let s_uop o = match o with T.MINUS -> "-" | NOT -> "!" in
  let s_arr (x, y) = x ^ "[" ^ y ^ "]" in
  match instr with
  | HALT -> "HALT"
  | SKIP -> "SKIP"
  | ALLOC   (x, n)       -> x ^ " = alloc (" ^ string_of_int n ^ ")"
  | ASSIGNV (x, o, y, z) -> x ^ " = " ^ y ^ " " ^ s_bop o ^ " " ^ z
  | ASSIGNC (x, o, y, n) -> x ^ " = " ^ y ^ " " ^ s_bop o ^ " " ^ string_of_int n
  | ASSIGNU (x, o, y)    -> x ^ " = " ^ s_uop o ^ y
  | COPY  (x, y) -> x ^ " = " ^ y
  | COPYC (x, n) -> x ^ " = " ^ string_of_int n
  | UJUMP  label  -> "goto "                   ^ string_of_int label
  | CJUMP  (x, l) -> "if "      ^ x ^ " goto " ^ string_of_int l
  | CJUMPF (x, l) -> "iffalse " ^ x ^ " goto " ^ string_of_int l
  | LOAD  (x, a) -> x       ^ " = " ^ s_arr a
  | STORE (a, x) -> s_arr a ^ " = " ^ x
  | READ  x -> "read "  ^ x
  | WRITE x -> "write " ^ x
let string_of_linstr (label, instr) = string_of_int label ^ " : " ^ string_of_instr instr


module ConstFolding = struct
  type cpo = Bot | Top | Num of int
  type ioset = (T.var, cpo) PMap.t
  type block = bool * ioset * ioset * T.linstr
  type blocks = block list

  module Str = struct
    let cpo c = match c with Bot -> "B" | Top -> "T" | Num n -> string_of_int n
    let ioset s = "{ " ^ PMap.foldi (fun v c s -> s ^ v ^ "->" ^ cpo c ^ "; ") s "" ^ "}"
    let block (f, ib, ob, st) = string_of_linstr st ^ "\tIN: " ^ ioset ib ^ "\tOUT: " ^ ioset ob
    let blocks blks = String.concat "\n" (List.map block blks)
  end

  let get_ctx (var : T.var) set = try  PMap.find var set
                                  with _ -> Bot

  let eval_unary : T.uop -> cpo -> cpo = fun op c ->
    match (c, op) with
    | Num n, MINUS -> Num (-n)
    | Num n, NOT   -> if n = 0 then Num 1 else Num 0
    | _ -> c

  let eval_binary : cpo -> T.bop -> cpo -> cpo = fun c1 op c2 ->
    match (c1, c2) with
    | Bot, _ | _, Bot -> Bot
    | Top, _ | _, Top -> Top
    | Num n, Num m -> (
      match T.eval_binary (NUM n) op (NUM m) with
      | NUM v -> Num v
      | _ -> raise UnreachableStatement )

  let union_ctx ctx1 ctx2 =
    match (ctx1, ctx2) with
    | Bot, c2 -> c2
    | c1, Bot -> c1
    | c1, c2 when c1 = c2 -> c1
    | _ -> Top

  let union_ioset : ioset -> ioset -> ioset = fun set1 set2 ->
    let union_element var ctx_n set =
      let ctx_p = get_ctx var set in
      PMap.add var (union_ctx ctx_p ctx_n) set in
    PMap.foldi union_element set1 set2

  let ioset_equiv : ioset -> ioset -> bool = fun set1 set2 ->
    let folder s v c r = r && c = get_ctx v s in
    PMap.foldi (folder set2) set1 true &&
    PMap.foldi (folder set1) set2 true

  (** calculate out_b from in_b. *)
  let propagate : T.instr -> ioset -> ioset = fun instr in_b ->
    match instr with
    | ALLOC   (x, _)         -> PMap.add x Top in_b
    | ASSIGNV (x, bop, y, z) -> let cy, cz = (get_ctx y in_b, get_ctx z in_b) in
                                PMap.add x (eval_binary cy bop cz) in_b
    | ASSIGNC (x, bop, y, n) -> let cy, cz = (get_ctx y in_b, Num n) in
                                PMap.add x (eval_binary cy bop cz) in_b
    | ASSIGNU (x, uop, y)    -> let cy = get_ctx y in_b in
                                PMap.add x (eval_unary uop cy) in_b
    | COPY  (x, y) -> PMap.add x (get_ctx y in_b) in_b
    | COPYC (x, n) -> PMap.add x (Num n) in_b
    | LOAD (x, _)
    | READ  x      -> PMap.add x Top in_b
    | _ -> in_b

  (** substitute instr if has constant results. *)
  let substitute : ioset -> T.instr -> T.instr = fun set instr ->
    match instr with
    | ASSIGNV (x, bop, y, z) ->
      ( match (get_ctx x set, bop, get_ctx y set, get_ctx z set) with
        | Num nx, _, _, _ -> COPYC (x, nx)
        | _, _, _, Num nz -> ASSIGNC (x, bop, y, nz)
        | _, (ADD | MUL | EQ | AND | OR), Num ny, _
                          -> ASSIGNC (x, bop, z, ny)
        | _, LT, Num ny, _ -> ASSIGNC (x, GT, z, ny)
        | _, LE, Num ny, _ -> ASSIGNC (x, GE, z, ny)
        | _, GT, Num ny, _ -> ASSIGNC (x, LT, z, ny)
        | _, GE, Num ny, _ -> ASSIGNC (x, LE, z, ny)
        | _ -> instr )
    | ASSIGNC (x, _, _, _)
    | ASSIGNU (x, _, _)
    | COPY  (x, _)
    | COPYC (x, _)  -> ( match get_ctx x set with Num m -> COPYC (x, m)                     | _ -> instr )
    | CJUMP  (x, l) -> ( match get_ctx x set with Num m -> if m != 0 then UJUMP l else SKIP | _ -> instr )
    | CJUMPF (x, l) -> ( match get_ctx x set with Num m -> if m =  0 then UJUMP l else SKIP | _ -> instr )
    | _ -> instr

  (** simplify unneed instr. *)
  let postprocess : T.instr -> T.instr = fun instr ->
    match instr with
    | ASSIGNC (x, (ADD | SUB), y, 0)
    | ASSIGNC (x, (MUL | DIV), y, 1)     -> COPY  (x, y)
    | ASSIGNC (x, AND, y, 0)             -> COPYC (x, 0)
    | ASSIGNC (x, OR , y, n) when n != 0 -> COPYC (x, 1)
    | _ -> instr

  let rec loop : T.l2pc -> blocks -> (int * int) list -> T.program = fun l2pc blks queue ->
    match queue with
    | (pc_p, pc_b) :: queue ->
        let  _,    _,   out_p,                _ = List.nth blks pc_p in
        let vs, in_b, o_out_b, (label_b, instr) = List.nth blks pc_b in
        let in_b = union_ioset out_p in_b in
        let out_b = propagate instr in_b in
        let pc_ns = next_pcs l2pc pc_b instr in
        let blks = set_nth pc_b (true, in_b, out_b, (label_b, instr)) blks in
        if   vs && ioset_equiv out_b o_out_b
        then loop l2pc blks queue
        else let queue = queue @ List.map (fun pc_n -> (pc_b, pc_n)) pc_ns in
             loop l2pc blks queue
    | [] ->
        List.map (fun (_, _, s, (l, i)) -> (l, postprocess (substitute s i))) blks

  let run : T.program -> T.program = fun t ->
    let l2pc = T.get_label2pc t in
    let blks = List.map (fun linstr -> (false, PMap.empty, PMap.empty, linstr)) t in
    loop l2pc blks [(0, 0)]
end


module CopyPropagation = struct
  type ioset = (T.var, T.var) PMap.t
  type block = bool * ioset * ioset * T.linstr
  type blocks = block list

  module Str = struct
    let ioset s = "{ " ^ PMap.foldi (fun v w str -> str ^ v ^ "<-" ^ w ^ "; ") s "" ^ "}"
    let block (f, ib, ob, st) = string_of_linstr st ^ "\tIN: " ^ ioset ib ^ "\tOUT: " ^ ioset ob
    let blocks blks = String.concat "\n" (List.map block blks)
  end

  let intersect_ioset : ioset -> ioset -> ioset = fun set1 set2 ->
    let insert_element v w set =
      if w = try PMap.find v set2 with _ -> ""
      then PMap.add v w set
      else set in
    PMap.foldi insert_element set1 PMap.empty

  let ioset_equiv : ioset -> ioset -> bool = fun set1 set2 ->
    let folder s v w r = r && w = try PMap.find v s with _ -> "" in
    PMap.foldi (folder set2) set1 true &&
    PMap.foldi (folder set1) set2 true

  let kill_var : T.var -> ioset -> ioset =
   fun var set -> PMap.foldi (fun v w s -> if   w = var
                                           then PMap.remove v s
                                           else s               ) set set

  (** calculate out_b from in_b. *)
  let propagate instr in_b =
    match instr with
    | T.ALLOC (x, _)
    | ASSIGNV (x, _, _, _)
    | ASSIGNC (x, _, _, _)
    | ASSIGNU (x, _, _)
    | COPYC   (x, _)
    | LOAD    (x, _)
    | READ     x  -> kill_var x in_b
    | COPY (x, y) -> PMap.add x y (kill_var x in_b)
    | _           -> in_b

  (** substitute instr if has copied variable arguments. *)
  let substitute : ioset -> T.instr -> T.instr = fun in_b instr ->
    let rec get_orig_var v =
      if   PMap.mem v in_b
      then get_orig_var (try PMap.find v in_b with _ -> "") 
      else v in
    match instr with
    | ASSIGNV (x, bop, y, z) -> ASSIGNV (x, bop, get_orig_var y, get_orig_var z)
    | ASSIGNC (x, bop, y, n) -> ASSIGNC (x, bop, get_orig_var y, n)
    | ASSIGNU (x, uop, y)    -> ASSIGNU (x, uop, get_orig_var y)
    | COPY   (x, y) -> COPY   (x, get_orig_var y)
    | CJUMP  (x, l) -> CJUMP  (get_orig_var x, l)
    | CJUMPF (x, l) -> CJUMPF (get_orig_var x, l)
    | LOAD  (x, (a, i)) -> LOAD  (x, (a, get_orig_var i))
    | STORE ((a, i), x) -> STORE ((a, get_orig_var i), get_orig_var x)
    | WRITE x -> WRITE (get_orig_var x)
    | _ -> instr

  let rec loop : T.l2pc -> blocks -> (int * int) list -> T.program = fun l2pc blks queue ->
    match queue with
    | (pc_p, pc_b) :: queue ->
        let  _,    _,   out_p,                _ = List.nth blks pc_p in
        let vs, in_b, o_out_b, (label_b, instr) = List.nth blks pc_b in
        let in_b = if   vs
                   then intersect_ioset out_p in_b
                   else out_p in
        let out_b = propagate instr in_b in
        let pc_ns = next_pcs l2pc pc_b instr in
        let blks = set_nth pc_b (true, in_b, out_b, (label_b, instr)) blks in
        if   vs && ioset_equiv out_b o_out_b
        then loop l2pc blks queue
        else let queue = queue @ List.map (fun pc_n -> (pc_b, pc_n)) pc_ns in
             loop l2pc blks queue
    | [] ->
        List.mapi (fun pc (_, ib, _, (l, i)) -> (l, substitute ib i)) blks

  let run : T.program -> T.program = fun t ->
    let l2pc = T.get_label2pc t in
    let blks = List.map (fun li -> (false, PMap.empty, PMap.empty, li)) t in
    loop l2pc blks [(0, 0)]
end


module CommonSubexpElimination = struct
  type ioset = (T.instr, T.var) PMap.t
  type block = bool * ioset * ioset * T.linstr
  type blocks = block list

  module Str = struct
    let ioset s = "{ " ^ PMap.foldi (fun li v str -> str ^ string_of_instr li ^ "\t-> " ^ v ^ "; ") s "" ^ "}"
    let block (f, ib, ob, st) = string_of_linstr st ^ "\tIN: " ^ ioset ib ^ "\tOUT: " ^ ioset ob
    let blocks blks = String.concat "\n" (List.map block blks)
  end

  let intersect_ioset : ioset -> ioset -> ioset = fun set1 set2 ->
    let insert_element v w set =
      if   w = try PMap.find v set2 with _ -> ""
      then PMap.add v w set
      else set in
    PMap.foldi insert_element set1 PMap.empty

  let ioset_equiv : ioset -> ioset -> bool = fun set1 set2 ->
    let folder s v w r = r && w = try PMap.find v s with _ -> "" in
    PMap.foldi (folder set2) set1 true &&
    PMap.foldi (folder set1) set2 true

  let kill_var : T.var -> ioset -> ioset = fun var set ->
    PMap.foldi (fun i v s ->
        if   v = var
        then PMap.remove i s
        else match i with
             | T.ASSIGNV (_, _, y, z) when y = var
                                      ||   z = var -> PMap.remove i s
             | ( ASSIGNC (_, _, y, _)
               | ASSIGNU (_, _, y)
               | COPY    (_, y)     ) when y = var -> PMap.remove i s
             | LOAD (_, (a, ai))      when a = var
                                      ||  ai = var -> PMap.remove i s
             | _ -> s
      ) set set

  (** calculate out_b from in_b. *)
  let propagate : T.instr -> ioset -> ioset = fun instr in_b ->
    let update i x set = if   PMap.mem i set
                         then set
                         else PMap.add i x set in
    match instr with
    | ASSIGNV (x, bop, y, z) -> update (T.ASSIGNV ("", bop, y, z)) x (kill_var x in_b)
    | ASSIGNC (x, bop, y, n) -> update (T.ASSIGNC ("", bop, y, n)) x (kill_var x in_b)
    | ASSIGNU (x, uop, y)    -> update (T.ASSIGNU ("", uop, y)   ) x (kill_var x in_b)
    | COPY  (x, y) -> update (T.COPY  ("", y)) x (kill_var x in_b)
    | COPYC (x, n) -> update (T.COPYC ("", n)) x (kill_var x in_b)
    | LOAD  (x, a) -> update (T.LOAD  ("", a)) x (kill_var x in_b)
    | READ x -> kill_var x in_b
    | _ -> in_b

  (** substitute instr if has copied variable arguments. *)
  let substitute : ioset -> T.instr -> T.instr = fun in_b instr ->
    let rec alternative i x si =
      let v = try PMap.find si in_b with _ -> "" in
      if v = "" then i else T.COPY (x, v) in
    match instr with
    | ASSIGNV (x, bop, y, z) -> alternative instr x (ASSIGNV ("", bop, y, z))
    | ASSIGNC (x, bop, y, n) -> alternative instr x (ASSIGNC ("", bop, y, n))
    | ASSIGNU (x, uop, y)    -> alternative instr x (ASSIGNU ("", uop, y)   )
    | COPY  (x, y) -> alternative instr x (COPY  ("", y))
    | COPYC (x, n) -> alternative instr x (COPYC ("", n))
    | LOAD  (x, a) -> alternative instr x (LOAD  ("", a))
    | _ -> instr

  let rec loop : T.l2pc -> blocks -> (int * int) list -> T.program = fun l2pc blks queue ->
    match queue with
    | (pc_p, pc_b) :: queue ->
        let  _,    _,   out_p,                _ = List.nth blks pc_p in
        let vs, in_b, o_out_b, (label_b, instr) = List.nth blks pc_b in
        let in_b = if   vs
                   then intersect_ioset out_p in_b
                   else out_p in
        let out_b = propagate instr in_b in
        let pc_ns = next_pcs l2pc pc_b instr in
        let blks = set_nth pc_b (true, in_b, out_b, (label_b, instr)) blks in
        if   vs && ioset_equiv out_b o_out_b
        then loop l2pc blks queue
        else let queue = queue @ List.map (fun pc_n -> (pc_b, pc_n)) pc_ns in
             loop l2pc blks queue
    | [] ->
        List.mapi (fun pc (_, ib, _, (l, i)) -> (l, substitute ib i)) blks

  let run : T.program -> T.program = fun t ->
    let l2pc = T.get_label2pc t in
    let blks = List.map (fun li -> (false, PMap.empty, PMap.empty, li)) t in
    loop l2pc blks [(0, 0)]
end


module DeadcodeElimination = struct
  type ioset = (T.var, bool) PMap.t
  type block = bool * ioset * ioset * T.linstr
  type blocks = block list
  type flowmap = (T.pc, T.pc list) PMap.t

  module Str = struct
    let ioset s = "{ " ^ PMap.foldi (fun v _ str -> str ^ v ^ "; ") s "" ^ "}"
    let block (f, ib, ob, st) = string_of_linstr st ^ "\t" ^ (if f then "v" else " ") ^ " IN: " ^ ioset ib ^ "\tOUT: " ^ ioset ob
    let blocks blks = String.concat "\n" (List.map block blks)
    let flowmap fm = "{\n"
      ^ PMap.foldi (fun t fl str -> str ^ Printf.sprintf "\t%d <- [ %s ]\n" t (String.concat "; " (List.map string_of_int fl))) fm ""
      ^ "}"
  end

  let get_revflow : T.l2pc -> T.program -> flowmap = fun l2pc pgm ->
    let pcs = List.mapi (fun pc _ -> pc) pgm in
    let flow = List.fold_left (fun f pc -> PMap.add pc [] f) PMap.empty pcs in
    let add pa pb fl = PMap.add pa (pb :: (try PMap.find pa fl with _ -> [])) fl in
    let rec loop check queue fl =
      match queue with
      | [] -> fl
      | pc :: queue -> (
          if   PMap.mem pc check
          then loop check queue fl
          else let check = PMap.add pc true check in
               let _, instr = List.nth pgm pc in
               match instr with
               | T.UJUMP l     -> let pc_n = get_pc_of_label l2pc l in
                                  loop check (queue @ [pc_n        ]) (add pc_n pc fl)
               | CJUMP  (_, l)
               | CJUMPF (_, l) -> let pc_n = get_pc_of_label l2pc l in
                                  loop check (queue @ [pc_n; pc + 1]) (add pc_n pc (add (pc + 1) pc fl))
               | HALT -> loop check queue fl
               | _    -> loop check (queue @ [pc + 1]) (add (pc + 1) pc fl)
      ) in
    loop PMap.empty [0] flow

  let get_halt_pc : T.l2pc -> T.program -> T.pc list = fun l2pc pgm ->
    let rec loop check queue res =
      match queue with
      | [] -> res
      | pc :: queue -> (
          if   PMap.mem pc check
          then loop check queue res
          else let check = PMap.add pc true check in
               let label, instr = List.nth pgm pc in
               match instr with
               | T.UJUMP l     -> loop check (get_pc_of_label l2pc l :: queue) res
               | CJUMP  (_, l)
               | CJUMPF (_, l) -> loop check (get_pc_of_label l2pc l :: (pc + 1) :: queue) res
               | HALT -> loop check queue (pc :: res)
               | _    -> loop check ((pc + 1) :: queue) res
      ) in
    loop PMap.empty [0] []

  let union_ioset : ioset -> ioset -> ioset = fun set1 set2 ->
    PMap.foldi (fun var _ set -> PMap.add var true set) set1 set2

  let ioset_equiv : ioset -> ioset -> bool = fun set1 set2 ->
    let folder s v _ r = r && PMap.mem v s in
    PMap.foldi (folder set2) set1 true &&
    PMap.foldi (folder set1) set2 true

  (** calculate in_b from out_b. *)
  let propagate : T.instr -> ioset -> ioset = fun instr out_b ->
    match instr with
    | ALLOC (x, _)
    | COPYC (x, _)         -> out_b |> PMap.remove x
    | ASSIGNV (x, _, y, z) -> out_b |> PMap.remove x |> PMap.add y true |> PMap.add z true
    | ASSIGNC (x, _, y, _)
    | ASSIGNU (x, _, y)
    | COPY    (x, y)       -> out_b |> PMap.remove x |> PMap.add y true
    | CJUMP  (x, _)
    | CJUMPF (x, _)        -> out_b |> PMap.add x true
    | LOAD  (x, (a, i))    -> out_b |> PMap.remove x   |> PMap.add a true |> PMap.add i true
    | STORE ((a, i), x)    -> out_b |> PMap.add x true |> PMap.add a true |> PMap.add i true
  (*| READ  x              -> out_b |> PMap.remove x *)
    | WRITE x              -> out_b |> PMap.add x true
    | _                    -> out_b

  (** check removability of instruction. *)
  let removable : ioset -> T.instr -> bool = fun out_b instr ->
    match instr with
    | ALLOC   (x, _)
    | ASSIGNV (x, _, _, _)
    | ASSIGNC (x, _, _, _)
    | ASSIGNU (x, _, _)
    | COPY  (x, _)
    | COPYC (x, _)
    | LOAD  (x, _) -> not (PMap.mem x out_b)
    | _            -> false

  let rec substitute : blocks -> T.program = fun blks ->
    List.fold_left (fun r (vs, _, ob, (l, i)) ->
        if      not vs               then r
        else if not (removable ob i) then r @ [(l, i)]
        else if l <> T.dummy_label   then r @ [(l, T.SKIP)]
        else                              r
      ) [] blks

  let rec loop : flowmap -> blocks -> (int * int) list -> T.program = fun flow blks queue ->
    match queue with
    | (pc_s, pc_b) :: queue ->
        let  _,   in_s,     _,                _ = List.nth blks pc_s in
        let vs, o_in_b, out_b, (label_b, instr) = List.nth blks pc_b in
        let out_b = union_ioset in_s out_b in
        let in_b = propagate instr out_b in
        let pc_ps = try PMap.find pc_b flow with _ -> [] in
        let blks = set_nth pc_b (true, in_b, out_b, (label_b, instr)) blks in
        if   vs && ioset_equiv in_b o_in_b
        then loop flow blks queue
        else let queue = queue @ List.map (fun pc_p -> (pc_b, pc_p)) pc_ps in
             loop flow blks queue
    | [] ->
        substitute blks

  let run_once : T.program -> T.program = fun t ->
    let l2pc = T.get_label2pc t in
    let flow = get_revflow l2pc t in
    let blks = List.map (fun li -> (false, PMap.empty, PMap.empty, li)) t in
    let queue = get_halt_pc l2pc t |> List.map (fun n -> (n, n)) in
    loop flow blks queue

  let rec run : T.program -> T.program = fun t ->
    let ol = List.length t in
    let t = run_once t in
    let al = List.length t in
    if ol = al then t else run t
end


module SimpleReduction = struct
  module D = DeadcodeElimination
  type reduce = Push | Merge of T.linstr

  module Str = struct
    let reduce r = match r with Push -> "SKIP" | Merge li -> "Merged " ^ string_of_linstr li
  end

  let rec substitute : D.blocks -> T.program -> T.program = fun revblks pgm ->
    match revblks with
    | [] -> pgm
    | [(_, _, _, li)] -> ( match li with
                           | l, SKIP when l = T.dummy_label -> pgm
                           | _                              -> li :: pgm )
    | blk_b :: blk_p :: rbs -> (
        let _,    _, ob_b, li_b = blk_b in
        let f, ib_p, ob_p, li_p = blk_p in
        let rflag =
          match (li_p, li_b) with
          | (l1, UJUMP l), (l2, i2) when l1 = T.dummy_label
                                    &&   l  = l2             -> Merge (l2, i2)
          |             _, (l2,  _) when l2 <> T.dummy_label -> Push
          |        li1, (_, SKIP) -> Merge li1
          | (l1, SKIP), (_,   i2) -> Merge (l1, i2)
          | (l1, ASSIGNV (t, op, y, z)), (_, COPY (x, t')) when t = t' && not (PMap.mem t ob_b) -> Merge (l1, ASSIGNV (x, op, y, z))
          | (l1, ASSIGNC (t, op, y, n)), (_, COPY (x, t')) when t = t' && not (PMap.mem t ob_b) -> Merge (l1, ASSIGNC (x, op, y, n))
          | (l1, ASSIGNU (t, op, y)   ), (_, COPY (x, t')) when t = t' && not (PMap.mem t ob_b) -> Merge (l1, ASSIGNU (x, op, y))
          | (l1, LOAD    (t, ar)      ), (_, COPY (x, t')) when t = t' && not (PMap.mem t ob_b) -> Merge (l1, LOAD    (x, ar))
          | _ -> Push in
        match rflag with
        | Merge li_n -> substitute ((f, ib_p, ob_b, li_n) :: rbs) pgm
        | Push       -> (
            match rbs with
            | []            -> substitute (blk_p :: rbs) (li_b :: pgm)
            | blkpp :: rbss -> (
                let fpp, ibpp, _, lipp = blkpp in
                match (lipp, li_p, li_b) with
                | (l1, CJUMP  (x, lt)), (l2, UJUMP lf), (l3, i3) when l2 = T.dummy_label && lt = l3 ->
                        let li_n = (l1, T.CJUMPF (x, lf)) in
                        substitute (blk_b :: (fpp, ibpp, ob_p, li_n) :: rbss) pgm
                | (l1, CJUMPF (x, lf)), (l2, UJUMP lt), (l3, i3) when l2 = T.dummy_label && lf = l3 ->
                        let li_n = (l1, T.CJUMP (x, lt)) in
                        substitute (blk_b :: (fpp, ibpp, ob_p, li_n) :: rbss) pgm
                | _  -> substitute (blk_p :: rbs) (li_b :: pgm)
              )
          )
      )

  let rec loop : D.flowmap -> D.blocks -> (int * int) list -> T.program = fun flow blks queue ->
    match queue with
    | (pc_s, pc_b) :: queue ->
        let  _,   in_s,     _,                _ = List.nth blks pc_s in
        let vs, o_in_b, out_b, (label_b, instr) = List.nth blks pc_b in
        let out_b = D.union_ioset in_s out_b in
        let in_b = D.propagate instr out_b in
        let pc_ps = try PMap.find pc_b flow with _ -> [] in
        let blks = set_nth pc_b (true, in_b, out_b, (label_b, instr)) blks in
        if   vs && D.ioset_equiv in_b o_in_b
        then loop flow blks queue
        else let queue = queue @ List.map (fun pc_p -> (pc_b, pc_p)) pc_ps in
             loop flow blks queue
    | [] ->
        substitute (List.rev blks) []

  let run : T.program -> T.program = fun t ->
    let l2pc = T.get_label2pc t in
    let flow = D.get_revflow l2pc t in
    let blks = List.map (fun li -> (false, PMap.empty, PMap.empty, li)) t in
    let queue = D.get_halt_pc l2pc t |> List.map (fun n -> (n, n)) in
    loop flow blks queue
end


let optimize : T.program -> T.program = fun t ->
  let rec loop t =
    let ol = List.length t in
    let t = t |> CopyPropagation.run |> DeadcodeElimination.run |> CommonSubexpElimination.run in
    let nl = List.length t in
    if ol = nl then t else loop t in
  t |> ConstFolding.run |> loop |> SimpleReduction.run
